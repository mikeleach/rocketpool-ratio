RPL Tracker

Because RPL must be staked at an ETH ratio for the operation of the Rocket Pool network, there is a minimum amount of ETH that each RPL is worth based on network data like number of minipools and amount of ETH staked. This script calculates that ratio floor as well as some second order calculations like speculation level and price/floor ratio (called the "multiplier"). 

Boodle first created this script to generate the this graph: https://imgur.com/a/EfMQKFR

Speculation Level = (price-floor)/price
Multiplier = price / floor

Discussion here: https://discord.com/channels/405159462932971535/405163713063288832/908222733907791882

Currently, input is manual, but this data exists on the Rocket Pool subgraph and could easily be automated: https://github.com/Data-Nexus/RocketPool-Subgraph-Goerli
