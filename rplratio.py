import numpy as np
import matplotlib.pyplot as plt
import datetime
import sys
import requests
from requests.structures import CaseInsensitiveDict
import time

# constants
apiurl = "https://api.coingecko.com/api/v3/coins/rocket-pool/history?date="

# make a grid for total bonded ETH and average collateralization (INSU)
POOLS,INSU = np.meshgrid(np.linspace(0.0,2,100)*1e5,np.linspace(0.0,1.5,100))


# Define function to calculate RPL/ETH ratio
# divide ETH by a factor of 2 because only half bonded ETH is collateralized
def get_floor(eth_staked,rpl_staked_perc,rpl_insurance,rpl_supply=1.8e7):
    rpl_staked = rpl_supply*rpl_staked_perc
    floor = (eth_staked*rpl_insurance)/rpl_staked
    return floor


class DataPoint:

    date = datetime.datetime.now() - datetime.timedelta(days = 1)
    average_collateral = 100
    num_minipools = 1

    # @boodle: help plz?
    def get_rpl_staked_perc():
        return 1.
    rpl_staked_perc = get_rpl_staked_perc()

    floor = get_floor(num_minipools*16,rpl_staked_perc,average_collateral)
    
    def get_price_on_date(date):
            headers = CaseInsensitiveDict()
            headers["accept"] = "application/json"
            finalurl = apiurl+date.strftime("%d-%m-%Y")
            response = requests.get(finalurl, headers=headers)
            
            if(response.status_code != 200):
                print("Coingecko API call failed!\n\n" + response) 
                sys.exit()

            # be nice to coingecko
            time.sleep(.5)        
            return response.json()["market_data"]["current_price"]["eth"]
    price = get_price_on_date(date)

    def get_multiplier(floor, price):
        return price / floor
    multiplier = get_multiplier(floor, price)

    def get_speculation_level(floor, price):
        return (price-floor)/price
    speculation_level = get_speculation_level(floor, price)

    def __init__(self, date, avg_collateral, num_minipools) -> None:
        self.date = date
        self.average_collateral = avg_collateral
        self.num_minipools = num_minipools
        print("--" + date.strftime("%Y-%m-%d") + "--\n" 
        + "\nmarket price: " + (f'{self.price:.5f}') + "ETH"
        + "\nfloor price: " + (f'{self.floor:.5f}') + "ETH"
        + "\nspeculation level: " + str(self.speculation_level) + "%"
        + "\nspeculative multiplier: " + (f'{self.multiplier:.2f}') + "x floor"
        + "\navg collateral: " + (f'{self.average_collateral:.1f}') + "%"
        + "\nnum minipools: " + str(self.num_minipools) 
        + "\n")



# ==== Plot the figure ====
fig = plt.figure()
ax0 = plt.subplot(111)
floor = get_floor(POOLS*16,1.,INSU)
CS = ax0.contour(INSU*100,POOLS/1000,floor,colors='k',linewidths=2,levels=np.array([0.01,0.1]))
clabel = plt.clabel(CS, inline=1, fontsize=14)
cf = ax0.contourf(INSU*100,POOLS/1000,floor,levels=np.arange(0,.28,0.01))
plt.colorbar(cf,ax=ax0,label='Floor ETH/RPL Ratio',ticks=np.arange(0,.28,0.025))
plt.xlabel('Average Minipool Collateralization %')
plt.ylabel('# Minipools (in thousands)')
plt.title('18M Circulating RPL')

# Plot a reference line for Lido
lido_eth = 1.4e6
lido_validators = lido_eth/32.
plt.axhline(lido_validators/1000.,c='indianred',ls='--')
plt.text(65,lido_validators/1000.+5.,'Lido Equivalent',c='indianred',fontweight='bold')

# ==== Import and plot data ====
filename = "rp.dat"
data = open(filename, "r")

for line in data.readlines():
    # skip comments
    if(line.startswith("#")):
        continue
    
    #read data
    data = line.split(",")
    date = datetime.datetime.fromisoformat(data[2].rstrip("\n"))
    
    try:    
        DataPoint(date, float(data[0]), data[1])
        
    except:
        print("Error parsing data for " + date)
        sys.exit()

    
    plt.plot(float(data[0]),float(data[1])/1000.,'r.',ms=10)


plt.plot(110,35./1000.,'r.',ms=10)

# Save the figure
plt.savefig('ETHRPL_Model.png',dpi=300)